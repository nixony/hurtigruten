import React from 'react';

const Exercise1 = () => {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-12 col-md-3">
                    <form>
                        <div className="search-group my-3">
                            <input type="text" className="py-2 px-3" placeholder="Search" />
                            <button className="search-btn"></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Exercise1;