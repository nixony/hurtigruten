import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Exercise3 = () => {
    const inputRef = React.useRef();
    const [search, setSearch] = useState('');
    const [debounceSearch, setDebounceSearch] = useState('search');
    const [results, setResults] = useState([]);

    useEffect(() => {
        const timerId = setTimeout(() => {
            setDebounceSearch(search);
        }, 400);

        return () => {
            clearTimeout(timerId);
        }
    }, [search]);

    useEffect(() => {
        const apiSearch = async () => {
            const { data } = await axios.get('http://localhost:4000/api/ships/' + debounceSearch);
            setResults(data);
        }
        
        if(debounceSearch.length > 0) {
            apiSearch();
        }

    }, [debounceSearch])
    
    const clearSearch = (e) => {
        e.preventDefault();
        setSearch('');
        setResults([])
        inputRef.current.focus();
    }

    const renderedResults = results.map((result) => {
        return(
            <div className="search-result" key={result.id}>
                { result.name }
            </div>
        )
    });
    
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-12 col-md-3">
                    <form onSubmit={ (e) => e.preventDefault() }>
                        <div className="search-group my-3">
                            <input
                                ref={inputRef}
                                value={search}
                                onChange={ (e) => setSearch(e.target.value) }
                                type="text" 
                                className="py-2 px-3" 
                                placeholder="Search" />
                            { 
                                search.length === 0
                                ? <button 
                                    className="search-btn"
                                    type="button"
                                ></button>
                                : <button 
                                    className="clear-btn"
                                    type="button"
                                    onClick={ (e) => clearSearch(e) }
                                ></button>
                            }
                        </div>
                    </form>
                    <div className="search-results">
                        { renderedResults }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Exercise3;