import React, { useState } from 'react';
import axios from 'axios';

const Exercise3 = () => {
    const inputRef = React.useRef();
    const [search, setSearch] = useState('');
    const [results, setResults] = useState([]);

    const clearSearch = (e) => {
        e.preventDefault();
        setSearch('');
        setResults([])
        inputRef.current.focus();
    }

    const submitSearch = async (e) => {
        e.preventDefault();
        const { data } = await axios.get('http://localhost:4000/api/ships/' + search);
        setResults(data);
    }

    const renderedResults = results.map((result) => {
        return(
            <div className="search-result" key={result.id}>
                { result.name }
            </div>
        )
    });
    
    
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-12 col-md-3">
                    <form onSubmit={ (e) => { submitSearch(e); }}>
                        <div className="search-group my-3">
                            <input
                                ref={inputRef}
                                value={search}
                                onChange={ (e) => setSearch(e.target.value) }
                                type="text" 
                                className="py-2 px-3" 
                                placeholder="Search" />
                            { 
                                search.length === 0
                                ? <button 
                                    className="search-btn"
                                    type="button"
                                ></button>
                                : <button 
                                    className="clear-btn"
                                    type="button"
                                    onClick={ (e) => clearSearch(e) }
                                ></button>
                            }
                        </div>
                    </form>
                    <div className="search-results">
                        { renderedResults }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Exercise3;