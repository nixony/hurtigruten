import React, { useState } from 'react';

const Exercise2 = () => {
    const inputRef = React.useRef();
    const [search, setSearch] = useState('');

    const clearSearch = (e) => {
        e.preventDefault();
        setSearch('');
        inputRef.current.focus();
    }
    
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-12 col-md-3">
                    <form>
                        <div className="search-group my-3">
                            <input
                                ref={inputRef}
                                value={search}
                                onChange={ (e) => setSearch(e.target.value) }
                                type="text" 
                                className="py-2 px-3" 
                                placeholder="Search" />
                            { 
                                search.length === 0
                                ? <button 
                                    className="search-btn"
                                ></button>
                                : <button 
                                    className="clear-btn"
                                    onClick={ (e) => clearSearch(e) }
                                ></button>
                            }
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Exercise2;